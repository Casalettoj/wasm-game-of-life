import React, { useState, useRef, useEffect, useReducer } from 'react';
import { Universe, Cell } from 'game_of_life';
import { memory } from "game_of_life/game_of_life_bg.wasm";

import "./style.scss";

const CELL_SIZE = 10; // px
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";
const DEFAULT_WIDTH = 64;
const DEFAULT_HEIGHT = 64;

export default function Counter(): JSX.Element {
  // https://reactjs.org/docs/hooks-overview.html
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const defaultSpeed = 100;
  const intervalRef = useRef<NodeJS.Timeout | undefined>();
  const [universe, setUniverse] = useState<Universe>(Universe.new(DEFAULT_WIDTH, DEFAULT_HEIGHT));
  const [intervalSpeed, setIntervalSpeed] = useState<number>(defaultSpeed);
  const [run, toggleRun] = useReducer((r) => !r, false);

  const getIndex = (row: number, column: number) => {
    return row * universe.width() + column;
  };

  const fps = new class {
    fps: HTMLElement | null;
    lastFrameTimeStamp: number;
    frames: number[];
  
    constructor() {
      this.fps = document.getElementById("fps");
      this.frames = [];
      this.lastFrameTimeStamp = performance.now();
    }
  
    render() {
      // Convert the delta time since the last frame render into a measure
      // of frames per second.
      const now = performance.now();
      const delta = now - this.lastFrameTimeStamp;
      this.lastFrameTimeStamp = now;
      const fps = 1 / delta * 1000;
  
      // Save only the latest 100 timings.
      this.frames.push(fps);
      if (this.frames.length > 100) {
        this.frames.shift();
      }
  
      // Find the max, min, and mean of our 100 latest timings.
      let min = Infinity;
      let max = -Infinity;
      let sum = 0;
      for (let i = 0; i < this.frames.length; i++) {
        sum += this.frames[i];
        min = Math.min(this.frames[i], min);
        max = Math.max(this.frames[i], max);
      }
      let mean = sum / this.frames.length;
  
      // Render the statistics.
      this.fps!.textContent = `
  Frames per Second:
           latest = ${Math.round(fps)}
  avg of last 100 = ${Math.round(mean)}
  min of last 100 = ${Math.round(min)}
  max of last 100 = ${Math.round(max)}
  `.trim();
    }
  };

  const drawGrid = (ctx: CanvasRenderingContext2D, width: number, height: number) => {
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;
  
    // Vertical lines.
    for (let i = 0; i <= width; i++) {
      ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
      ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
    }
  
    // Horizontal lines.
    for (let j = 0; j <= height; j++) {
      ctx.moveTo(0,                           j * (CELL_SIZE + 1) + 1);
      ctx.lineTo((CELL_SIZE + 1) * width + 1, j * (CELL_SIZE + 1) + 1);
    }
  
    ctx.stroke();
  };

  const drawCells = (ctx: CanvasRenderingContext2D, width: number, height: number) => {
    const cellsPtr = universe.cells();
    const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);
  
    ctx.beginPath();
  
    for (let row = 0; row < height; row++) {
      for (let col = 0; col < width; col++) {
        const idx = getIndex(row, col);
  
        ctx.fillStyle = cells[idx] === Cell.Dead
          ? DEAD_COLOR
          : ALIVE_COLOR;
  
        ctx.fillRect(
          col * (CELL_SIZE + 1) + 1,
          row * (CELL_SIZE + 1) + 1,
          CELL_SIZE,
          CELL_SIZE
        );
      }
    }
  
    ctx.stroke();
  };

  useEffect(() => {
    const ctx = canvasRef.current!.getContext('2d')!;
    const universeHeight = universe.height();
    const universeWidth = universe.width();
    if (run) {
      intervalRef.current && clearInterval(intervalRef.current);
      intervalRef.current = setInterval(() => {
        universe.tick();
        drawCells(ctx, universeWidth, universeHeight);
        drawGrid(ctx, universeWidth, universeHeight);
        fps.render();
      }, intervalSpeed);
    } else {
      intervalRef.current && clearInterval(intervalRef.current);
      drawCells(ctx, universeWidth, universeHeight);
      drawGrid(ctx, universeWidth, universeHeight);
    }
  }, [run, intervalSpeed, universe]);

  const toggleCell = (event: React.MouseEvent<HTMLElement>) => {
    const boundingRect = canvasRef.current!.getBoundingClientRect();

    const scaleX =  canvasRef.current!.width / boundingRect.width;
    const scaleY =  canvasRef.current!.height / boundingRect.height;

    const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
    const canvasTop = (event.clientY - boundingRect.top) * scaleY;

    const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), canvasRef.current!.height - 1);
    const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), canvasRef.current!.width - 1);

    universe.toggle_cell(row, col);

    const ctx = canvasRef.current!.getContext('2d')!;
    drawGrid(ctx,  universe.width(), universe.height());
    drawCells(ctx,  universe.width(), universe.height());
  }

  return (
    <div className="game-of-life">
      <p>Click to toggle a cell.</p>
      <button onClick={toggleRun}> {(run && "Stop") || "Start"}</button><span>&nbsp; <span id="fps"></span></span>
      <button onClick={() => {setUniverse(Universe.new(universe.width(), universe.height()))}}> Reset </button>
      <br /><br />
      <button onClick={() => {setIntervalSpeed((prev) => prev*2) }}>Slower</button>
      <button onClick={() => {setIntervalSpeed(defaultSpeed) }}>Normal Speed</button>
      <button onClick={() => {setIntervalSpeed((prev) => prev/2) }}>Faster</button>
      <canvas ref={canvasRef} height={(CELL_SIZE + 1) * universe.height() + 1} width={(CELL_SIZE + 1) * universe.width() + 1} onClick={toggleCell} />
    </div>
  );
}
